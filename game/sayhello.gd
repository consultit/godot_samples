extends Panel

var accum = 0
var s
var scene = preload("res://child_button.tscn")

func _on_button_pressed():
	get_node("Label").set_text("HELLO!")

func _ready():
	get_node("Label/Button").connect("pressed",self,"_on_button_pressed")
	set_process(true)
	get_tree().call_group(0, "parent", "call_parent")
	for n in get_tree().get_nodes_in_group("parent"):
		print(n.get_name())
	s = Sprite.new()
	add_child(s)
	add_child(scene.instance())
	
func _process(delta):
	accum += delta
	get_node("Label/Button").set_text(str(accum))
	
func call_parent():
	print("all parent are called")

func _notification(what):
	if (what == NOTIFICATION_READY):
		print("This is the same as overriding _ready()...")
	elif (what == NOTIFICATION_PROCESS):        
		var delta = get_process_delta_time()
		print("This is the same as overriding _process()...")