extends Node2D

var screen_size
var pad_size=Vector2()
var direction = Vector2(1.0, 0.0)
const INITIAL_BALL_SPEED = 80
var ball_speed = INITIAL_BALL_SPEED
const PAD_SPEED = 150
#mouse
var status = null
var mpos=Vector2()
const DRAG = PAD_SPEED * 0.2
#touchscreen
var left_status = "released"
var right_status = "released"
var left_idx = null
var right_idx = null
var left_drag_pos = Vector2()
var right_drag_pos = Vector2()
var k = 1.5

func _ready():
	set_process(true)
	set_process_input(true)
	screen_size = get_viewport_rect().size
	pad_size = get_node("left").get_texture().get_size()

func _process(delta):
	var ball_pos = get_node("ball").get_pos()
	var left_rect = Rect2( get_node("left").get_pos() - pad_size*0.5, pad_size )
	var right_rect = Rect2( get_node("right").get_pos() - pad_size*0.5, pad_size )
	ball_pos += direction * ball_speed * delta
	if ((ball_pos.y < 0 and direction.y < 0) or (ball_pos.y > screen_size.y and direction.y > 0)):
		direction.y = -direction.y
	if ((left_rect.has_point(ball_pos) and direction.x < 0) or (right_rect.has_point(ball_pos) and direction.x > 0)):
		direction.x = -direction.x
		direction.y = randf()*2.0 - 1
		direction = direction.normalized()
		ball_speed *= 1.1
	if (ball_pos.x < 0 or ball_pos.x > screen_size.x):
		ball_pos = screen_size*0.5
		ball_speed = INITIAL_BALL_SPEED
		direction = Vector2(-1, 0)
	get_node("ball").set_pos(ball_pos)
	var left_pos = get_node("left").get_pos()
	if (left_pos.y > 0 and Input.is_action_pressed("left_move_up")):
		left_pos.y += -PAD_SPEED * delta
	if (left_pos.y < screen_size.y and Input.is_action_pressed("left_move_down")):
		left_pos.y += PAD_SPEED * delta
	var right_pos = get_node("right").get_pos()
	if (right_pos.y > 0 and Input.is_action_pressed("right_move_up")):
		right_pos.y += -PAD_SPEED * delta
	if (right_pos.y < screen_size.y and Input.is_action_pressed("right_move_down")):
		right_pos.y += PAD_SPEED * delta
	# touchscreen
	if left_status == "dragging" or left_status == "clicked":
		var oldpos = left_pos
		left_pos.y += delta * DRAG * (left_drag_pos - left_pos).y
		if (left_pos.y <= 0) or (left_pos.y >= screen_size.y):
			left_pos.y = oldpos.y
	if right_status == "dragging" or right_status == "clicked":
		var oldpos = right_pos
		right_pos.y += delta * DRAG * (right_drag_pos - right_pos).y
		if (right_pos.y <= 0) or (right_pos.y >= screen_size.y):
			right_pos.y = oldpos.y
	# mouse
#	if status == "left_dragging":
#		var oldpos = left_pos
#		var mposcurr = get_tree().get_root().get_mouse_pos()
#		left_pos.y += delta * (2 * mposcurr - mpos - oldpos).y * DRAG 
#		mpos = mposcurr
#		if (left_pos.y <= 0) or (left_pos.y >= screen_size.y):
#			left_pos.y = oldpos.y
#	if status == "right_dragging":
#		var oldpos = right_pos
#		var mposcurr = get_tree().get_root().get_mouse_pos()
#		right_pos.y += delta * (2 * mposcurr - mpos - oldpos).y * DRAG 
#		mpos = mposcurr
#		if (right_pos.y <= 0) or (right_pos.y >= screen_size.y):
#			right_pos.y = oldpos.y
	# update positions
	get_node("left").set_pos(left_pos)
	get_node("right").set_pos(right_pos)

func _input(ev):
	# touchscreen
	if ev.type == InputEvent.SCREEN_TOUCH and ev.is_pressed():
		if left_status != "dragging":
			var pos = get_node("left").get_pos() - pad_size * 0.5
			var left_rect = Rect2(Vector2(pos.x - k * pad_size.y, pos.y), Vector2(pad_size.x + pad_size.y * k, pad_size.y))
			if left_rect.has_point(ev.pos):
				left_status="clicked"
				left_idx = ev.index
				left_drag_pos = ev.pos
		if right_status != "dragging":
			var pos = get_node("right").get_pos() - pad_size * 0.5
			var right_rect = Rect2(pos, Vector2(pad_size.x + pad_size.y * k, pad_size.y))
			if right_rect.has_point(ev.pos):
				right_status="clicked"
				right_idx = ev.index
				right_drag_pos = ev.pos
	if ev.type==InputEvent.SCREEN_DRAG:
		if ev.index==left_idx:
			if left_status=="clicked":
				left_status="dragging"
			left_drag_pos = ev.pos
		if ev.index==right_idx:
			if right_status=="clicked":
				right_status="dragging"
			right_drag_pos = ev.pos
	if ev.type == InputEvent.SCREEN_TOUCH and (not ev.is_pressed()):
		print("released")
		if ev.index==left_idx and left_status!="released":
			print("left")
			left_status="released"
		if ev.index==right_idx and right_status!="released":
			print("right")
			right_status="released"
	#mouse
#	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_LEFT and \
#			ev.is_pressed() and (status != "left_dragging" or status != "right_dragging"):
#		var evpos=get_tree().get_root().get_mouse_pos()
#		var left_rect = Rect2( get_node("left").get_pos() - pad_size*0.5, pad_size )
#		var right_rect = Rect2( get_node("right").get_pos() - pad_size*0.5, pad_size )
#		if left_rect.has_point(evpos):
#			status="left_clicked"
#			mpos = evpos
#		if right_rect.has_point(evpos):
#			status="right_clicked"
#			mpos = evpos
#	if ev.type == InputEvent.MOUSE_MOTION and status=="left_clicked":
#		status="left_dragging"
#	if ev.type == InputEvent.MOUSE_MOTION and status=="right_clicked":
#		status="right_dragging"
#	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_LEFT and \
#			(status=="left_dragging" or status=="right_dragging"):
#		if not ev.is_pressed():
#			status="released"

# debug
func print_ev_touch(ev):
	print("SCREEN_TOUCH", \
		"(type:",ev.type,")", \
		"(ID:",ev.ID,")", \
		"(device:",ev.device,")", \
		"(index:",ev.index,")", \
		"(pos:",ev.pos,")", \
		"(x:",ev.x,")", \
		"(y:",ev.y,")", \
		"(pressed:",ev.pressed,")", \
		"(is_pressed():",ev.is_pressed(),")")

func print_ev_drag(ev):
	print("SCREEN_DRAG", \
		"(type:",ev.type,")", \
		"(ID:",ev.ID,")", \
		"(device:",ev.device,")", \
		"(index:",ev.index,")", \
		"(pos:",ev.pos,")", \
		"(x:",ev.x,")", \
		"(y:",ev.y,")", \
		"(relative_pos:",ev.relative_pos,")", \
		"(relative_x:",ev.relative_x,")", \
		"(relative_y:",ev.relative_y,")", \
		"(speed:",ev.speed,")", \
		"(speed_x:",ev.speed_x,")", \
		"(speed_y:",ev.speed_y,")")

## gesture detector
const touchSlop = 1 #from configuration
const doubleTapTouchSlop = 1 #from configuration
const doubleTapSlop = 1 #from configuration
var mTouchSlopSquare
var mDoubleTapTouchSlopSquare
var mDoubleTapSlopSquare
const mMinimumFlingVelocity = 1 #from configuration
const mMaximumFlingVelocity = 1 #from configuration

const LONGPRESS_TIMEOUT = 1 #from configuration
const TAP_TIMEOUT = 1 #from configuration
const DOUBLE_TAP_TIMEOUT = 1 #from configuration
const DOUBLE_TAP_MIN_TIME = 1 #from configuration

# constants for Message.what used by GestureHandler below
const SHOW_PRESS = 1
const LONG_PRESS = 2
const TAP = 3

var mStillDown
var mDeferConfirmSingleTap
var mInLongPress
var mInContextClick
var mAlwaysInTapRegion
var mAlwaysInBiggerTapRegion
var mIgnoreNextUpEvent

var mCurrentDownEvent
var mPreviousUpEvent
var mIsDoubleTapping

var mLastFocusX
var mLastFocusY
var mDownFocusX
var mDownFocusY

var mIsLongpressEnabled

func init_gesture_detector():
	mIsLongpressEnabled = true
	mTouchSlopSquare = touchSlop * touchSlop
	mDoubleTapTouchSlopSquare = doubleTapTouchSlop * doubleTapTouchSlop
	mDoubleTapSlopSquare = doubleTapSlop * doubleTapSlop
	
func _input_gesture_detector(ev):
	if ev.type == InputEvent.SCREEN_TOUCH and ev.is_pressed():
		pass