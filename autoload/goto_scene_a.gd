extends Button

func _enter_tree():
	print (get_name() + "._enter_tree")

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	print (get_name() + "._ready")

func _exit_tree():
	print (get_name() + "._exit_tree")
