
extends Panel

# member variables here, example:
# var a=2
# var b="textvar"

func _enter_tree():
	print (get_name() + "._enter_tree")

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	print (get_name() + "._ready")

func _exit_tree():
	print (get_name() + "._exit_tree")

func _on_goto_scene_pressed():
	get_node("/root/global").goto_scene("res://scene_a.scn")
