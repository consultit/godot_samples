extends Panel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var time_elapsed = 0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("Button").connect("pressed",self,"_on_button_pressed")
	set_process(true)
	add_to_group("main_scenes")

func _on_button_pressed():
    get_node("Label").set_text("HELLO!")
    get_tree().call_group(0, "main_scenes", "print_status")

func print_status():
	var main_scenes = get_tree().get_nodes_in_group("main_scenes")
	for i in main_scenes:
		print(i.get_name())
	print("button pressed")

func _process(delta):
    time_elapsed += delta
    get_node("Label").set_text(str(time_elapsed))
